import flask
import requests 
import json
from flask import request, jsonify

app = flask.Flask(__name__)
app.config["DEBUG"] = True



@app.route('/apiDetails', methods=['GET'])
def apidetails():
    return '''<h1>API Details</h1>
<p>1: Provide city name to get current weather details. For Example Url: http://127.0.0.1:5000/weather/?cityname=bangalore
2: Provide city name with Date in yyyy/mm/dd format to get weather details.For Example Url: http://127.0.0.1:5000/weather/?cityname=bangalore&datetemp=2019/08/12/ </p>'''


@app.route('/notinuse/', methods=['GET'])
def getWeatherDetailsByCityName():
    
    if 'cityname' in request.args:
        print ("not in use:")
        cityname =(request.args['cityname'])
        URL = "https://www.metaweather.com/api/location/search/"
        location = cityname
        PARAMS = {'query':location}
        r = requests.get(url = URL, params = PARAMS, verify=False)
        data = r.json()
        person_dict = json.dumps(data)
        # print("person_dict: ",person_dict) 

        resp = json.loads(person_dict)

        
        # print ("Final 2:",resp[0]['woeid'])
        woeid=int(resp[0]['woeid'])
        print ("woeid finally:",woeid)
		
        URLFinal = "https://www.metaweather.com/api/location/"
        print ("1")
        PARAMSFinal = {'query':woeid}
        print ("2")
        completeurl=URLFinal+str(woeid)
        print ("completeurl:", completeurl)
        rFinal = requests.get(completeurl, verify=False)
        print ("3", rFinal)
        dataFinal = rFinal.json()
        # print ("dataFinal :",dataFinal)
        person_dictFinal = json.dumps(dataFinal)
        # print("person_dict: ",person_dict) 

        respFinal = json.loads(person_dictFinal)
        return jsonify(respFinal)
			
    else:
        return "Error: No name field provided. Please specify an name."
        
        

@app.route('/weather/', methods=['GET'])
def getWeatherDetailsByCityNameWithDate():
    
    if 'cityname' in request.args:
        if 'datetemp' in request.args:
            print ("only name and date is there")
            cityname =(request.args['cityname'])
            datetemp =(request.args['datetemp'])
            URL = "https://www.metaweather.com/api/location/search/"
            location = cityname
            PARAMS = {'query':location}
            r = requests.get(url = URL, params = PARAMS, verify=False)
            data = r.json()
            person_dict = json.dumps(data)
            # print("person_dict: ",person_dict) 

            resp = json.loads(person_dict)

        
            # print ("Final 2:",resp[0]['woeid'])
            woeid=int(resp[0]['woeid'])
            print ("woeid finally:",woeid)
		
            URLFinal = "https://www.metaweather.com/api/location/"
            print ("1")
            PARAMSFinal = {'query':woeid}
            print ("2")
            completeurl=URLFinal+str(woeid)+'/'+str(datetemp)
            print ("completeurl:", completeurl)
            rFinal = requests.get(completeurl, verify=False)
            print ("3", rFinal)
            dataFinal = rFinal.json()
            # print ("dataFinal :",dataFinal)
            person_dictFinal = json.dumps(dataFinal)
            # print("person_dict: ",person_dict) 

            respFinal = json.loads(person_dictFinal)
            return jsonify(respFinal)
        else:
              print ("only name is there")
              cityname =(request.args['cityname'])
              URL = "https://www.metaweather.com/api/location/search/"
              location = cityname
              PARAMS = {'query':location}
              r = requests.get(url = URL, params = PARAMS, verify=False)
              data = r.json()
              person_dict = json.dumps(data)
              # print("person_dict: ",person_dict) 

              resp = json.loads(person_dict)

        
              # print ("Final 2:",resp[0]['woeid'])
              woeid=int(resp[0]['woeid'])
              print ("woeid finally:",woeid)
		
              URLFinal = "https://www.metaweather.com/api/location/"
              print ("1")
              PARAMSFinal = {'query':woeid}
              print ("2")
              completeurl=URLFinal+str(woeid)
              print ("completeurl:", completeurl)
              rFinal = requests.get(completeurl, verify=False)
              print ("3", rFinal)
              dataFinal = rFinal.json()
              # print ("dataFinal :",dataFinal)
              person_dictFinal = json.dumps(dataFinal)
              # print("person_dict: ",person_dict) 

              respFinal = json.loads(person_dictFinal)
              return jsonify(respFinal)
			
    else:
        return "Error: No name field provided. Please specify an name."        

    

    
    

app.run()