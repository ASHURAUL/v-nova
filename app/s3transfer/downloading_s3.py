import boto3

s3 = boto3.client('s3')
s3.download_file('BUCKET_NAME', 'OBJECT_NAME', 'FILE_NAME')

# The download_file method accepts the names of the bucket and object to download and the filename to save the file to.